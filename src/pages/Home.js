import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
//import background from "../App.css";

export default function Home() {
  let sampleProp = "This is a sample prop.";

  let bannerData = {
    title: "Adriano's Little Italy",
    //description: "Come inside to view our high-quality products!",
    buttonText: "View Our Products",
    destination: "/products",
  };

  return (
    <>
      <div className="home" />
      <Banner bannerProp={bannerData} />
      <Highlights HighlightsProp={sampleProp} />
    </>
  );
}

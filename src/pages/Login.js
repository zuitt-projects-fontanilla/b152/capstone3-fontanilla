import { useState, useContext, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import { Navigate } from 'react-router-dom';

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setisActive] = useState(false);

  useEffect(() => {
    if (email !== '' && password !== '') {
      setisActive(true);
    } else {
      setisActive(false);
    }
  }, [email, password]);

  function loginUser(e) {
    e.preventDefault();

    fetch(
      'https://centralized-atrainism.onrender.com/users/login',
      /*"https://odd-rose-bass-veil.cyclic.app/users/login",*/ {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },

        body: JSON.stringify({
          email: email,
          password: password,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.accessToken) {
          Swal.fire({
            icon: 'success',
            title: 'Login Successful',
            text: 'Thank you for logging in',
          });
          localStorage.setItem('token', data.accessToken);

          let token = localStorage.getItem('token');
          console.log(token);

          fetch(
            'https://centralized-atrainism.onrender.com/users/getUserDetails',
            /*'https://odd-rose-bass-veil.cyclic.app/users/getUserDetails',*/ {
              method: 'GET',
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
            .then((res) => res.json())
            .then((data) => {
              setUser({
                id: data._id,
                isAdmin: data.isAdmin,
              });
            });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Login Failed',
            text: data.message,
          });
        }
      });
  }

  return user.id ? (
    <Navigate to="/products" replace={true} />
  ) : (
    <>
      <h1 className="page-title mt-0 mb-5 text-center">Login</h1>
      <Form
        className="col-lg-4 col-sm-4 offset-md-4"
        onSubmit={(e) => loginUser(e)}>
        <Form.Group>
          <Form.Label className="text-white col-12">
            Email:
            <Form.Control
              name="email"
              type="email"
              autoComplete="off"
              placeholder="Enter Email"
              required
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>
        <Form.Group>
          <Form.Label className="text-white col-12">
            Password
            <Form.Control
              name="password"
              type="password"
              autoComplete="off"
              placeholder="Enter Password"
              required
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>
        {isActive ? (
          <Button variant="dark" type="submit" className="my-5">
            Submit
          </Button>
        ) : (
          <Button variant="dark" disabled className="my-5">
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}

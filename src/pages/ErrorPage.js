import Banner from "../components/Banner";

export default function ErrorPage() {
  let errorBanner = {
    title: "Page Not Found",
    buttonText: "Back to Home",
    destination: "/",
  };

  return (
    <>
      <div className="home" />
      <Banner bannerProp={errorBanner} className="mt-5" />;
    </>
  );
}

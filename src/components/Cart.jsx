import PropTypes from "prop-types";
import { useContext } from "react";
import { CartContext } from "../context/cart.jsx";

export default function Cart(props) {
  const { cartItems, addToCart, removeFromCart, clearCart, getCartTotal } =
    useContext(CartContext);

  return (
    props.showModal && (
      <div
        id="modale"
        className="position-fixed top-50 start-50 h-75 w-m-75 w-75 overflow-auto translate-middle bg-white rounded-3">
        <h1 className="text-center pt-5">Cart</h1>
        <div className="button-close">
          <button
            className="px-4 py-2 bg-black text-white rounded"
            onClick={() => props.toggle()}>
            Close
          </button>
        </div>
        <div className="flex flex-col">
          {cartItems.map((item) => (
            <div className="cart-item d-flex" key={item._id}>
              <div className="d-flex my-4 gap-3">
                <img
                  src={item.url}
                  alt={item.name}
                  className="cart-image rounded-md h-24"
                />
                <div className="flex flex-col">
                  <h4 className="mx-2">{item.name}</h4>
                  <div className="counter mt-3 d-flex gap-2">
                    <button
                      className="cart-button px-auto py-auto bg-black text-white rounded"
                      onClick={() => {
                        removeFromCart(item);
                      }}>
                      -
                    </button>
                    <p className="pt-1">{item.quantity}</p>
                    <button
                      className="cart-button px-auto py-auto bg-black text-white rounded"
                      onClick={() => {
                        addToCart(item);
                      }}>
                      +
                    </button>
                  </div>
                </div>
              </div>
              <div className="my-4 py-1 px-0 px-sm-3">
                <h5>${item.price}</h5>
              </div>
            </div>
          ))}
        </div>
        {cartItems.length > 0 ? (
          <>
            <div className="total d-flex mx-6 justify-content-between">
              <div className="button-css py-3">
                <button
                  className="px-2 px-md-4 py-2 bg-black text-white rounded"
                  onClick={() => {
                    clearCart();
                  }}>
                  Clear cart
                </button>
              </div>
              <div>
                <h4 className="flex pt-4 px-1 px-sm-3">
                  Total: ${getCartTotal()}
                </h4>
              </div>
            </div>
            <div className="checkout d-flex mb-5 justify-content-end">
              <button className="checkout-button px-2 px-md-4 py-2 text-white rounded">
                Checkout
              </button>
            </div>
          </>
        ) : (
          <h4 className="text-center">Your cart is empty</h4>
        )}
      </div>
    )
  );
}

Cart.propTypes = {
  showModal: PropTypes.bool,
  toggle: PropTypes.func,
};

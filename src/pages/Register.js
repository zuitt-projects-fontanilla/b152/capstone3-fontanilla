import { useState, useContext, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import { Navigate } from 'react-router-dom';

export default function Register() {
  const { user } = useContext(UserContext);

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const [isActive, setisActive] = useState(false);

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      password !== '' &&
      mobileNo.length === 11 &&
      password === confirmPassword
    ) {
      setisActive(true);
    } else {
      setisActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

  function registerUser(e) {
    e.preventDefault();

    fetch(
      'https://centralized-atrainism.onrender.com/users',
      /*"https://odd-rose-bass-veil.cyclic.app/users/",*/ {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobileNo: mobileNo,
          password: password,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.email) {
          Swal.fire({
            icon: 'success',
            title: 'Registration Successful',
            text: 'Thank you for registering!',
          });

          window.location.href = '/login';
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Registration Failed',
            text: 'Something Went Wrong.',
          });
        }
      });
  }

  return user.id ? (
    <Navigate to="/products" replace={true} />
  ) : (
    <>
      <h1 className="page-title mt-0 mb-5 text-center">Register</h1>
      <Form
        className="col-lg-4 col-sm-4 offset-md-4"
        onSubmit={(e) => registerUser(e)}>
        <Form.Group>
          <Form.Label className="text-white col-12">
            First Name:
            <Form.Control
              name="name"
              type="text"
              autoComplete="off"
              placeholder="Enter First Name"
              required
              value={firstName}
              onChange={(e) => {
                setFirstName(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>

        <Form.Group>
          <Form.Label className="text-white col-12">
            Last Name:
            <Form.Control
              name="lastName"
              type="text"
              placeholder="Enter Last Name"
              required
              value={lastName}
              onChange={(e) => {
                setLastName(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>

        <Form.Group>
          <Form.Label className="text-white col-12">
            Email:
            <Form.Control
              name="email"
              type="email"
              autoComplete="off"
              placeholder="Enter Email"
              required
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>

        <Form.Group>
          <Form.Label className="text-white col-12">
            Mobile No:
            <Form.Control
              name="number"
              type="number"
              placeholder="Enter 11 Digit No"
              required
              value={mobileNo}
              onChange={(e) => {
                setMobileNo(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>

        <Form.Group>
          <Form.Label className="text-white col-12">
            Password:
            <Form.Control
              name="password"
              type="password"
              autoComplete="off"
              placeholder="Enter Password"
              required
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>

        <Form.Group>
          <Form.Label className="text-white col-12">
            Confirm Password:
            <Form.Control
              name="confirm"
              type="password"
              autoComplete="off"
              placeholder="Confirm Password"
              required
              value={confirmPassword}
              onChange={(e) => {
                setConfirmPassword(e.target.value);
              }}
            />
          </Form.Label>
          <br />
        </Form.Group>
        {isActive ? (
          <Button variant="dark" type="submit" className="my-5">
            Submit
          </Button>
        ) : (
          <Button variant="dark" disabled>
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}

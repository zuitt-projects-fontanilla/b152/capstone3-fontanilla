import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Table, Button } from 'react-bootstrap';

export default function AdminDashboard() {
  const { user } = useContext(UserContext);

  const [allProducts, setAllProducts] = useState([]);

  function archive(productId) {
    fetch(
      `https://centralized-atrainism.onrender.com/products/archive/${productId}`, //`https://odd-rose-bass-veil.cyclic.app/products/archive/${productId}`,
      {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        window.location.href = '/products';
      });
  }

  function activate(productId) {
    fetch(
      `https://centralized-atrainism.onrender.com/products/activate/${productId}`, //`https://odd-rose-bass-veil.cyclic.app/products/activate/${productId}`,
      {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        window.location.href = '/products';
      });
  }

  useEffect(() => {
    if (user.isAdmin) {
      fetch(
        'https://centralized-atrainism.onrender.com/products/getAllProducts/',
        /*'https://odd-rose-bass-veil.cyclic.app/products/getAllProducts/',*/ {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          setAllProducts(
            data.map((product) => {
              return (
                <tr key={product._id}>
                  <td>{product._id}</td>
                  <td>{product.name}</td>
                  <td>{product.price}</td>
                  <td>{product.isActive ? 'Active' : 'Inactive'}</td>
                  <td>
                    {product.isActive ? (
                      <Button
                        variant="danger"
                        className="mx-2"
                        onClick={() => {
                          archive(product._id);
                        }}>
                        Archive
                      </Button>
                    ) : (
                      <Button
                        variant="success"
                        className="mx-2"
                        onClick={() => {
                          activate(product._id);
                        }}>
                        Activate
                      </Button>
                    )}
                  </td>
                </tr>
              );
            })
          );
        });
    }
  }, [user.isAdmin]);

  return (
    <>
      <h1 className="page-title mb-5 pt-6 text-center">Admin Dashboard</h1>
      <Table striped bordered hover className="bg=white">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{allProducts}</tbody>
      </Table>
    </>
  );
}

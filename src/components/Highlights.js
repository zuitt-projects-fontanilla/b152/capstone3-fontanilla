import { Row, Col, Card } from "react-bootstrap";

export default function Highlights({ HighlightsProp }) {
  return (
    <Row className="my-3">
      <Col xs={10} md={4} className="mx-auto mb-2">
        <Card className="p-3 cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Pasta, Cheese & Sauces from Scratch</h2>
            </Card.Title>
            <Card.Text>
              Take a gander at our home made selections of pastas, cheeses and
              sauces made from scratch and fresh out of mio mama Sylvia's
              Kitchen!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={10} md={4} className="mx-auto mb-2">
        <Card className="p-3 cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Today's Specials To Go</h2>
            </Card.Title>
            <Card.Text>
              Our specials are made with the highest quality ingredients found
              in the Fontanilla Farm or mio papa Mike's garden. Everyday we
              share with you authentic Italian recipes passed down through the
              Fontanilla generations. Mangiamo!!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={10} md={4} className="mx-auto mb-2">
        <Card className="p-3 cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Authentic Italian Wine</h2>
            </Card.Title>
            <Card.Text>
              Our collection of red, white and rosé wines has won awards from
              all over the world. Whether you like light, full bodied, sweet or
              dry, we have what you're looking for.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

import { useState, useEffect, useContext } from 'react';
import ProductCard from '../components/Product';
import AdminDashboard from '../components/AdminDashboard';
import { Row, Spinner, Alert } from 'react-bootstrap';
import UserContext from '../userContext';

export default function Products() {
  const { user } = useContext(UserContext);

  const [productsArray, setProductsArray] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  // useEffect(() => {
  //   fetch(
  //     'https://centralized-atrainism.onrender.com/products/getActiveProducts/' /*"https://odd-rose-bass-veil.cyclic.app/products/getActiveProducts"*/
  //   )
  //     .then((res) => res.json())
  //     .then((data) => {
  //       setProductsArray(
  //         data.map((product) => {
  //           return <ProductCard key={product._id} productProp={product} />;
  //         })
  //       );
  //       setLoading(false);
  //     });
  //     .catch((error) => {
  //       setError(error.message);
  //       setLoading(false);
  //     })
  // }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          'https://centralized-atrainism.onrender.com/products/getActiveProducts/' /*"https://odd-rose-bass-veil.cyclic.app/products/getActiveProducts"*/
        );
        if (!response.ok) {
          throw new Error('Failed to fetch products');
        }
        const data = await response.json();
        setProductsArray(
          data.map((product) => (
            <ProductCard key={product._id} productProp={product} />
          ))
        );
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };
    fetchData();
  });

  return user.isAdmin ? (
    <AdminDashboard />
  ) : (
    <>
      <h1 className="page-title mt-0 mb-5 text-center">Products</h1>
      {loading ? (
        <div className="spinner-container">
          <Spinner
            className="spinner"
            animation="border"
            variant="light"
            role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      ) : error ? (
        <Alert variant="danger">{error}</Alert>
      ) : (
        <Row className="d-flex flex-row flex-wrap justify-content-center text-center">
          {productsArray}
        </Row>
      )}
    </>
  );
}

import { useState, useEffect, useContext } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { CartContext } from '../context/cart';

export default function ViewSingleProduct() {
  const { productId } = useParams();
  const { addToCart } = useContext(CartContext);

  const [productDetails, setProductDetails] = useState({
    _id: null,
    name: null,
    description: null,
    price: null,
    url: null,
  });

  useEffect(() => {
    fetch(
      `https://centralized-atrainism.onrender.com/products/getSingleProduct/${productId}` /*`https://odd-rose-bass-veil.cyclic.app/products/getSingleProduct/${productId}`*/
    )
      .then((res) => res.json())
      .then((data) => {
        setProductDetails({
          _id: data._id,
          name: data.name,
          description: data.description,
          price: data.price,
          url: data.url,
        });
      });
  }, [productId]);

  return (
    <Row className="col-lg-4 col-md-6 col-10 mx-auto offset-md-3">
      <Col className="pt-5 mt-5">
        <Card className="single-card">
          <Card.Body className="text-center">
            <Card.Title>{productDetails.name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{productDetails.description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>${productDetails.price}</Card.Text>
            <Card.Img id="single-card-img" src={productDetails.url} />
            <div>
              <button
                onClick={() => addToCart(productDetails)}
                className="btn btn-dark mt-3 px-3">
                Add To Cart
              </button>
            </div>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

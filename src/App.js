import { useState, useEffect } from 'react';

import { Container } from 'react-bootstrap';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import AppNavBar from './components/AppNavBar';

import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/ErrorPage';
import Logout from './pages/Logout';
import ViewSingleProduct from './pages/ViewSingleProduct';

import AddProduct from './pages/AddProduct';
import { UserProvider } from './userContext';
import ScrollToTop from './components/ScrollToTop';
import './App.css';

export default function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const [showModal, setShowModal] = useState(false);

  const toggle = () => {
    setShowModal(!showModal);
    if (!showModal) {
      document.body.style.overflow = 'hidden';
      document.body.style.height = '100%';
      document.getElementById('overlay').style.display = 'block';
    } else {
      document.body.style.overflow = 'auto';
      document.getElementById('overlay').style.display = 'none';
    }
  };

  useEffect(() => {
    fetch(
      /*"https://odd-rose-bass-veil.cyclic.app/users/getUserDetails",*/ 'https://centralized-atrainism.onrender.com/users/getUserDetails',
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  }, []);

  const unsetUser = () => {
    localStorage.clear();
  };

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <div className="background-image" />
          <AppNavBar toggle={toggle} showModal={showModal} />
          <ScrollToTop />

          <Container className="content">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route
                path="/products/ViewSingleProduct/:productId"
                element={<ViewSingleProduct />}
              />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/addProduct" element={<AddProduct />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<ErrorPage />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

import { useContext } from "react";
import { CartContext } from "../context/cart";
import { Card } from "react-bootstrap";

import { Link } from "react-router-dom";

export default function ProductCard({ productProp }) {
  const { addToCart } = useContext(CartContext);

  return (
    <Card className="col-lg-4 col-md-6 col-10 m-5 cardProduct">
      <Card.Body className="my=2 ">
        <Card.Title>{productProp.name}</Card.Title>
        <Card.Text>{productProp.description}</Card.Text>
        <Card.Text>Price: ${productProp.price}</Card.Text>
        <Card.Img className="card-img" src={productProp.url} />
        <div className="d-flex justify-content-center mb-2">
          <Link
            to={`/products/ViewSingleProduct/${productProp._id}`}
            className="btn btn-dark mt-4 mx-2">
            View Product
          </Link>
          <div>
            <button
              onClick={() => addToCart(productProp)}
              className="btn btn-dark mt-4 px-3 mx-2">
              Add To Cart
            </button>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
}

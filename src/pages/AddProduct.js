import { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function AddProduct() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [url, setUrl] = useState('');

  function createProduct(e) {
    e.preventDefault();

    let token = localStorage.getItem('token');
    console.log(token);

    fetch(
      'https://centralized-atrainism.onrender.com/products',
      /*"https://odd-rose-bass-veil.cyclic.app/products/",*/ {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },

        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          url: url,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        localStorage.setItem('id', data._id);
        localStorage.setItem('isAdmin', data.isAdmin);

        if (data._id) {
          Swal.fire({
            icon: 'success',
            title: 'Product Creation Successful',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Product Creation Failed',
            text: data.message,
          });
        }
      });
  }

  return (
    <>
      <h1 className="page-title mb-5 pt-6 text-center">Add Product</h1>
      <Form
        className="col-lg-4 col-sm-4 offset-md-4"
        onSubmit={(e) => createProduct(e)}>
        <Form.Group>
          <Form.Label className="text-white">
            Name:
            <Form.Control
              name="name"
              type="text"
              autoComplete="off"
              placeholder="Enter Name"
              required
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>
        <Form.Group>
          <Form.Label className="text-white">
            Description:
            <Form.Control
              name="description"
              type="text"
              placeholder="Enter Description"
              required
              value={description}
              onChange={(e) => {
                setDescription(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>
        <Form.Group>
          <Form.Label className="text-white">
            Price:
            <Form.Control
              name="price"
              type="number"
              placeholder="Enter Price"
              required
              value={price}
              onChange={(e) => {
                setPrice(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>
        <Form.Group>
          <Form.Label className="text-white">
            Image URL:
            <Form.Control
              name="image"
              type="text"
              placeholder="Enter URL"
              required
              value={url}
              onChange={(e) => {
                setUrl(e.target.value);
              }}
            />
          </Form.Label>
        </Form.Group>
        <Button variant="dark" type="submit" className="my-5">
          Submit
        </Button>
      </Form>
    </>
  );
}

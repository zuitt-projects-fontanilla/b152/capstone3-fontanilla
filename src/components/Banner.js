import { Row, Col } from "react-bootstrap";

export default function Banner({ bannerProp }) {
  return (
    <Row>
      <Col className="content p-5">
        <h1 className="title mb-3">{bannerProp.title}</h1>
        <p className="text-white my-3">{bannerProp.description}</p>
        <a
          href={bannerProp.destination}
          className="d-flex button-main btn btn-dark mx-auto">
          {bannerProp.buttonText}
        </a>
      </Col>
    </Row>
  );
}

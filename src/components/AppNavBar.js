import { useContext, useRef } from "react";
import { Nav, Navbar, Container } from "react-bootstrap";
import UserContext from "../userContext";
import { Link } from "react-router-dom";
import { CartContext } from "../context/cart";
import Cart from "./Cart";

export default function AppNavBar(props) {
  const { user } = useContext(UserContext);
  const { cartItems } = useContext(CartContext);
  const linksContainerRef = useRef(null);

  function collapseNav() {
    linksContainerRef.current.classList.remove("show");
  }

  const getQuantity = () => {
    return cartItems.reduce((total, item) => total + item.quantity, 0);
  };

  return (
    <>
      <div id="overlay"></div>
      <Navbar className="navbar position-fixed w-100" bg="dark" expand="lg">
        <Container fluid>
          <Navbar.Brand href="/" className="text-white">
            Adriano's Little Italy
          </Navbar.Brand>

          <Navbar.Collapse id="basic-navbar-nav" ref={linksContainerRef}>
            <Nav className="ml-auto">
              <Link
                to="/"
                className="nav-link text-white"
                onClick={collapseNav}>
                Home
              </Link>
              <Link
                to="/products"
                className="nav-link text-white"
                onClick={collapseNav}>
                Products
              </Link>
              {user.id ? (
                user.isAdmin ? (
                  <>
                    <Link
                      to="/addProduct"
                      className="nav-link text-white"
                      onClick={collapseNav}>
                      Add Product
                    </Link>
                    <Link
                      to="/logout"
                      className="nav-link text-white"
                      onClick={collapseNav}>
                      Logout
                    </Link>
                  </>
                ) : (
                  <Link
                    to="/logout"
                    className="nav-link text-white"
                    onClick={collapseNav}>
                    Logout
                  </Link>
                )
              ) : (
                <>
                  <Link
                    to="/register"
                    className="nav-link text-white"
                    onClick={collapseNav}>
                    Register
                  </Link>
                  <Link
                    to="/login"
                    className="nav-link text-white"
                    onClick={collapseNav}>
                    Login
                  </Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
          <div className="cart-parent d-flex">
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <div
              onClick={() => props.toggle()}
              type="submit"
              className="cart-icon">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="25"
                height="25"
                fill="white"
                className="bi bi-bag-fill"
                viewBox="0 0 16 16"
                id="safari">
                <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5z" />
              </svg>
            </div>
            <span>
              <span className="cart-length">{getQuantity()}</span>
            </span>
          </div>
        </Container>
      </Navbar>
      <Cart showModal={props.showModal} toggle={props.toggle} />
    </>
  );
}
